#include <stdio.h>
#include "dia.h"

/*Se ingresa un día del año e imprime el dia de la semana que le corresponde*/
int main (void)
{
	int a, b, c;
	printf(INGRESE_1);
	scanf("%d", & a);

	for(a = a; a < 1 || a > 366; a = a) /*Corrobora que se trate de un numero válido*/
	{
		printf(INGRESE_2);
		scanf("%d", & a);
	}
	
	for (b = a; b % 7 != 0; b--); /*Disminuye el valor del día hasta encontrar el que sea multiplo de 7*/
	c = a - b; /*Le resta al numero del día el valor encontrado en el for, resultando un valor entre 0 y 6*/
	switch (c) /*Le asigna a cada día de la semana el numero correspondente y lo imprime*/
	{
		case 0: printf(SAB);
		break;
		case 6: printf(VIER);
		break;
		case 5: printf(JUE);
		break;
		case 4: printf(MIER);
		break;
		case 3: printf(MAR);
		break;
		case 2: printf(LUN);
		break;
		case 1: printf(DOM);
	}
	return 0;
}
