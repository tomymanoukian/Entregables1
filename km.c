#include <stdio.h>
#include "km.h"

/* Calcula el costo y rendimiento de sucesivas cargas de combustible en un auto */
int main (void)
{
	int a;
	double rendimiento, rend, dist, comb, cost, precio, prom, mejor, peor, dtotal, comtotal, costotal;
	rendimiento = dtotal = comtotal = costotal = 0;
	
	for(a = 0; a >= 0; a++) /* Repite el mensaje indefinidamente */
	{
		printf(KILOMETROS);
		scanf("%lf", &dist);
		if (dist == -1) break; /* Termina el lazo de itineración del for al ingresar -1 */
		printf(PRECIO);
		scanf("%lf", &precio);
		printf(MONTO);
		scanf("%lf", &cost);
		comb = cost / precio;
		rend = (comb * 100)/dist; /* Calcula el rendimiento */
		printf(RENDIMIENTO ">> %.2f\n", rend);
		
		rendimiento = rendimiento + rend; /* Suma los rendimientos y los guarda */
		if (a == 0) peor = mejor = rend; /* Inicializa las variales */
		else
		{
			if (mejor > rend) mejor = rend; /* Busca el mejor rendimiento */
			if (peor < rend) peor = rend; /* Busca el peor rendimiento */
		}
		dtotal = dtotal + dist; /* Suma los kilometros recorridos */
		comtotal = comtotal + comb;/* Suma el combustible utilizado */
		costotal = costotal + cost;/* Suma los costos de cada carga */
				
		printf("\n");
	}
	printf("\n");
	
	prom = rendimiento / a; /* Calcula un promedio de todos los rendimientos */
	
	printf(RENDIMIENTO_PROM ">> %.4f\n", prom);
	printf(MEJOR_REND ">> %.2f\n", mejor);
	printf(PEOR_REND ">> %.2f\n", peor);
	printf(DISTANCIA ">> %.0f\n", dtotal);
	printf(COMBUSTIBLE ">> %.2f\n", comtotal);
	printf(COSTO ">> %.2f\n", costotal);
	return 0;
}
